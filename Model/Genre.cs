﻿using System.Collections.Generic;

namespace WebApplication2.Model
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Books { get; set; }

        public override bool Equals(object obj)
        {
            bool isEqual;

            if (obj == null || obj is not Genre)
            {
                isEqual = false;
            }
            else
            {
                var genre = obj as Genre;
                isEqual = this.Name.ToLower() == genre.Name.ToLower();
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
}

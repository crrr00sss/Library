﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace WebApplication2.Model
{
    public class LibraryDbContext : DbContext, ILibraryDb
    {
        public DbSet<Author> DbAuthors { get; set; }
        public DbSet<Book> DbBooks { get; set; }
        public DbSet<Genre> DbGenres { get; set; }
        public IList<Author> Authors => DbAuthors.ToList();
        public IList<Genre> Genres => DbGenres.ToList();

        public List<Author> AddAuthors(List<Author> authors)
        {
            var unqiueAuthor = authors.
                Where(a => DbAuthors.AsEnumerable().All(a2 => !a2.Equals(a))).
                ToList();

            DbAuthors.AddRange(unqiueAuthor);

            return authors.Except(unqiueAuthor).ToList();
        }

        public void ChangeAuthorInfo(int id, Author author)
        {
            var authorInDb = this.GetAuthorById(id);

            authorInDb.FirstName = author.FirstName?? authorInDb.FirstName;
            authorInDb.Patronymic = author.Patronymic ?? authorInDb.Patronymic;
            authorInDb.LastName = author.LastName ?? authorInDb.LastName;
            authorInDb.DateOfBirth = author.DateOfBirth;
        }

        public List<Genre> AddGenres(List<Genre> genres)
        {
            var unqiueGenres = genres.
                Where(g => DbGenres.AsEnumerable().All(g2 => !g2.Equals(g))).
                ToList();

            DbGenres.AddRange(unqiueGenres);

            return genres.Except(unqiueGenres).ToList();
        }

        public void DeleteAuthor(int id)
        {
            var author = GetAuthorById(id);

            DbBooks.RemoveRange(author.Books);
            DbAuthors.Remove(author);
        }

        public void DeleteBook(int id)
        {
            var book = DbBooks
                .Where(b => b.Id == id)
                .FirstOrDefault();

            DbBooks.Remove(book);
        }

        public Author GetAuthorById(int id)
        {
            return DbAuthors
                .Where(a => a.Id == id)
                .Include(a => a.Books)
                .ThenInclude(b => b.Genre)
                .FirstOrDefault();
        }

        public void DeleteGenre(int id)
        {
            var genre = DbGenres
                .Where(g => g.Id == id)
                .Include(g => g.Books)
                .FirstOrDefault();

            DbBooks.RemoveRange(genre.Books);
            DbGenres.Remove(genre);
        }

        public List<Book> GetBooksByGenre(int id)
        {
            return DbBooks
                .Where(b => b.GenreId == id)
                .Include(b => b.Genre)
                .Include(b => b.Author)
                .ToList();
        }

        public Genre GetGenreById(int id)
        {
            return DbGenres
                .Where(g => g.Id == id)
                .FirstOrDefault();
        }

        public void ChangeBookData(Book book)
        {
            var existBook = DbBooks.Find(book.Id);
            existBook.Title = book.Title;
            existBook.Genre = book.Genre;
            existBook.PageQuantity = book.PageQuantity;
        }

        public List<Book> AddBooks(List<Book> books, int authorId)
        {
            List<Book> notAddedBooks = new();
            var author = GetAuthorById(authorId);

            foreach (var book in books)
            {
                book.Genre = DbGenres
                    .Where(g => g.Name == book.Genre.Name)
                    .FirstOrDefault();

                book.Author = author;

                if (book.Id > 0)
                {
                    ChangeBookData(book);
                }
                else
                {
                    var isUnqiueBook = DbBooks
                        .Include(b => b.Author)
                        .AsEnumerable()
                        .All(b => !b.Equals(book));

                    if (isUnqiueBook)
                    {
                        DbBooks.Add(book);
                    }
                    else
                    {
                        notAddedBooks.Add(book);
                    }
                }
            }

            return notAddedBooks;
        }

        public void Save()
        {
            this.SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=LibraryDb;Trusted_Connection=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                .ToTable("Genres")
                .Property(a => a.Name)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Author>()
                .ToTable("Authors")
                .Property(a => a.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Author>()
                .Property(a => a.Patronymic)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Author>()
                .Property(a => a.LastName)
                .IsRequired()
                .HasMaxLength(50);

            modelBuilder.Entity<Author>()
                .Property(a => a.DateOfBirth)
                .HasColumnType("Date");

            modelBuilder.Entity<Book>()
                .ToTable("Books")
                .Property(a => a.Title)
                .IsRequired()
                .HasMaxLength(100);
        }


    }
}

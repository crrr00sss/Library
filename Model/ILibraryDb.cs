﻿using System.Collections.Generic;

namespace WebApplication2.Model
{
    public interface ILibraryDb
    {
        IList<Author> Authors { get; }
        IList<Genre> Genres { get; }
        Author GetAuthorById(int authorId);
        List<Author> AddAuthors(List<Author> authors);
        void ChangeAuthorInfo(int id, Author author);
        List<Genre> AddGenres(List<Genre> genres);
        void DeleteAuthor(int id);
        void DeleteBook(int id);
        void DeleteGenre(int id);
        List<Book> GetBooksByGenre(int id);
        Genre GetGenreById(int id);
        void ChangeBookData(Book book);
        List<Book> AddBooks(List<Book> books, int authorId);
        void Save();
    }
}

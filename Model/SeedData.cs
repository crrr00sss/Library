﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplication2.Model
{
    public class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            LibraryDbContext context = app.ApplicationServices
                .CreateScope().ServiceProvider.GetRequiredService<LibraryDbContext>();

            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }

            if (!context.DbGenres.Any())
            {
                context.DbGenres.AddRange(
                    new Genre { Name = "Сучасна проза" },
                    new Genre { Name = "Трагедія" },
                    new Genre { Name = "Комедія" },
                    new Genre { Name = "Драма" },
                    new Genre { Name = "Роман" }
                    );
            }

            if (!context.DbAuthors.Any())
            {
                context.DbAuthors.AddRange(
                    new Author
                    {
                        FirstName = "Юрій",
                        Patronymic = "Ігорович",
                        LastName = "Андрухович",
                        DateOfBirth = new DateTime(1960, 03, 13)
                    },
                    new Author
                    {
                        FirstName = "Ірина",
                        Patronymic = "Іванівна",
                        LastName = "Чернова",
                        DateOfBirth = new DateTime(1957, 10, 03)
                    },
                    new Author
                    {
                        FirstName = "Любомир",
                        Patronymic = "Андрійович",
                        LastName = "Дереш",
                        DateOfBirth = new DateTime(1984, 08, 10)
                    },
                    new Author
                    {
                        FirstName = "Сергій",
                        Patronymic = "Вікторович",
                        LastName = "Жадан",
                        DateOfBirth = new DateTime(1974, 08, 23)
                    },
                    new Author
                    {
                        FirstName = "Оксана",
                        Patronymic = "Стефанівна",
                        LastName = "Забужко",
                        DateOfBirth = new DateTime(1960, 09, 19)
                    }
                    );
                context.SaveChanges();
            }

            if (!context.DbBooks.Any())
            {
                context.DbBooks.AddRange(
                    new Book
                    {
                        Title = "Коханці Юстиції",
                        PageQuantity = 304,
                        Genre = context.DbGenres.Where(g => g.Name == "Сучасна проза").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Андрухович").FirstOrDefault()
                    },
                    new Book
                    {
                        Title = "Ініціація",
                        PageQuantity = 412,
                        Genre = context.DbGenres.Where(g => g.Name == "Сучасна проза").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Чернова").FirstOrDefault()
                    },
                    new Book
                    {
                        Title = "Спустошення",
                        PageQuantity = 576,
                        Genre = context.DbGenres.Where(g => g.Name == "Сучасна проза").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Дереш").FirstOrDefault()
                    },
                    new Book
                    {
                        Title = "Інтернат",
                        PageQuantity = 336,
                        Genre = context.DbGenres.Where(g => g.Name == "Сучасна проза").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Жадан").FirstOrDefault()
                    },
                    new Book
                    {
                        Title = "Музей покинутих секретів",
                        PageQuantity = 832,
                        Genre = context.DbGenres.Where(g => g.Name == "Сучасна проза").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Забужко").FirstOrDefault()
                    },
                    new Book
                    {
                        Title = "Рекреації",
                        PageQuantity = 240,
                        Genre = context.DbGenres.Where(g => g.Name == "Роман").FirstOrDefault(),
                        Author = context.Authors.Where(a => a.LastName == "Андрухович").FirstOrDefault()
                    }
                    );
                context.SaveChanges();
            }
        }
    }
}

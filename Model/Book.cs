﻿using System.Collections.Generic;

namespace WebApplication2.Model
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int PageQuantity { get; set; }
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }

        public override bool Equals(object obj)
        {
            bool isEqual;

            if (obj == null || obj is not Book)
            {
                isEqual = false;
            }
            else
            {
                var book = obj as Book;
                isEqual = this.Title.ToLower() == book.Title.ToLower() &&
                    this.PageQuantity == book.PageQuantity &&
                    this.Genre.Equals(book.Genre) &&
                    this.Author.Equals(book.Author);
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return this.Title.GetHashCode() ^ this.PageQuantity.GetHashCode() ^ this.Genre.GetHashCode();
        }
    }
}

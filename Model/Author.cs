﻿using System;
using System.Collections.Generic;

namespace WebApplication2.Model
{
    public class Author
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public List<Book> Books { get; set; }

        public override bool Equals(object obj)
        {
            bool isEqual;

            if (obj == null || obj is not Author)
            {
                isEqual = false;
            }
            else
            {
                var author = obj as Author;
                isEqual = this.FirstName.ToLower() == author.FirstName.ToLower() &&
                    this.Patronymic.ToLower() == author.Patronymic.ToLower() &&
                    this.LastName.ToLower() == author.LastName.ToLower() &&
                    this.DateOfBirth == author.DateOfBirth;
            }

            return isEqual;
        }

        public override int GetHashCode()
        {
            return this.FirstName.GetHashCode() ^ this.Patronymic.GetHashCode() ^ this.LastName.GetHashCode() ^ this.DateOfBirth.GetHashCode();
        }
    }
}

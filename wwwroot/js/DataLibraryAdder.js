﻿class DataLibraryAdder {
    pendingElements = [];
    isSomethingChange = false;
    temporaryElementForm;

    constructor(elementName, elementNameShow,) {
        this.elementName = elementName;
        this.elementNameShow = elementNameShow;
        this.createFormBtn = document.querySelector("#add-" + this.elementName);
    }

    start() {
        this.createFormBtn.addEventListener("click", () => this.createForm());
        this.validateInputs();
    }

    checkIfTemporaryFormInUse() {
        let tempForm = document.querySelector(".temporary-block");

        return Boolean(tempForm);
    }

    submitForm(e) {
        if (!this.isSomethingChange) {
            e.preventDefault();
            alert("Вы не обновляли данные");
            return;
        }

        let isCorrect = this.temporaryElementForm?.validateInputsOnSubmit();

        if (isCorrect) {
            let inputList = document.forms[0].querySelector(".listToUpdate");
            inputList.value = JSON.stringify(this.pendingElements);
        }
    }

    createSubmitBtn() {
        let submit = document.querySelector(".save-btn");
        if (!submit) {
            submit = document.createElement("input");
            submit.type = "submit";
            submit.value = "Сохранить";
            submit.classList.add("pull-right");
            submit.classList.add("save-btn");
            document.forms[0].prepend(submit);
            document.forms[0].addEventListener("submit", (e) => this.submitForm(e));
        }
    }

    createForm() {
        if (!this.checkIfTemporaryFormInUse()) {
            this.temporaryElementForm = new TemporaryFormCreator("add-" + this.elementName + "-template", this.elementNameShow);
            this.temporaryElementForm.show();
            let elementPendBtn = this.temporaryElementForm.getAddFormBtn();
            elementPendBtn.addEventListener("click", () => {
                let element = this.temporaryElementForm.pendFormToSave();

                if (element) {
                    this.pendingElements.push(element);
                    this.isSomethingChange = true;
                    this.createSubmitBtn();
                }
            });
        }
    }

    validateInputs() {
        let inputs = document.querySelectorAll("[data-val='true']");

        inputs.forEach(input => {
            input.addEventListener("change", () => {
                this.isSomethingChange = true;
                this.createSubmitBtn();
                let formInputValidator = new FormInputValidator(input);
                formInputValidator.validate();
            });
        });
    }
}
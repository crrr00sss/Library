﻿class DataLibraryUpdater {
    constructor(confirmMessage, elementName1, elementName2) {
        this.deleteBtns = document.querySelectorAll(".delete-btn");
        this.editBtns = document.querySelectorAll(".edit-btn");
        this.elementName1 = elementName1;
        this.elementName2 = elementName2;
        this.confirmMessage = confirmMessage;
    }

    start() {
        this.deleteBtns.forEach(
            btn => btn.addEventListener("click",
                () => this.deleteLibraryElement(btn)
            )
        );

        this.editBtns.forEach(
            btn => btn.addEventListener("click",
                () => this.editLibraryElement(btn)
            )
        );
    }

    deleteLibraryElement(btn) {
        let confirmDeletion = confirm(this.confirmMessage);

        if (confirmDeletion) {
            let form = document.createElement("form");
            form.classList.add("hidden-element");

            form.method = "POST";
            form.action = "/Home/Delete" + this.elementName1;

            let input1 = this.createInputElement(btn, this.elementName1);
            form.append(input1);

            if (this.elementName2 != undefined) {
                let input2 = this.createInputElement(btn, this.elementName2);
                form.append(input2);
            }

            document.body.append(form);

            form.submit();
        }
    }

    editLibraryElement(btn) {
        let elementId = btn.dataset[this.elementName1 + "Id"];

        btn.parentNode.classList.add("hidden-element");
        let editForm = new DataLibraryAdder(this.elementName1, "Книга");
        editForm.createForm();

        let idInput = document.createElement("input");
        idInput.name = this.elementName1 + "Id";
        idInput.dataset.object = "Id";
        idInput.classList.add("hidden-element");
        idInput.value = elementId;
        document.querySelector(".temporary-block").append(idInput);

        document.forms[0].title.value = btn.parentNode.querySelector(".property:nth-child(1)>div:nth-child(2)").textContent;
        document.forms[0].genre.value = btn.parentNode.querySelector(".property:nth-child(2)>div:nth-child(2)").textContent;
        document.forms[0].pageQty.value = btn.parentNode.querySelector(".property:nth-child(3)>div:nth-child(2)").textContent;

        editForm.temporaryElementForm.cancelBtn.addEventListener("click", (e) => {
            btn.parentNode.classList.remove("hidden-element");
        });
    }

    createInputElement(btn, elementName) {
        let element = document.createElement("input");
        let elementId = btn.dataset[elementName + "Id"];
        element.name = elementName + "Id";
        element.value = elementId;

        return element;
    }
}
﻿class TemporaryFormCreator {
    constructor(templateId, elementTypeName) {
        this.template = document.querySelector("#" + templateId);
        this.form = this.template.content.cloneNode(true).firstElementChild;
        this.elementTypeName = elementTypeName;
    }

    show() {
        this.form.querySelector("h1").textContent = this.elementTypeName + " на добавление";
        this.form.classList.add("temporary-block");
        this.template.after(this.form);

        this.form.querySelectorAll("button.hidden-element").forEach(element => element.classList.remove("hidden-element"));

        this.cancelBtn = this.form.querySelector(".btn-cancel");

        this.cancelBtn.addEventListener("click", () => this.cancelForm());

        this.validateInput();
    }

    getAddFormBtn() {
        return this.form.querySelector(".btn-add");
    }

    pendFormToSave() {
        let isCorrect = true;

        let inputsToValidate = this.form.querySelectorAll("[data-val='true']");
        let objectToAdd = {};

        inputsToValidate.forEach(input => {
            let formInputValidator = new FormInputValidator(input);
            isCorrect = formInputValidator.validate() && isCorrect;
        });

        let inputs = this.form.querySelectorAll("[data-object]");
        inputs.forEach(input => {
            let objectProperty = input.dataset.object.split(".");

            if (objectProperty.length > 1) {
                objectToAdd[objectProperty[0]] = {};
                objectToAdd[objectProperty[0]][objectProperty[1]] = input.value;
            }
            else {
                objectToAdd[input.dataset.object] = input.value;
            }
        });

        if (isCorrect) {
            this.changeToPermanentForm();
            return objectToAdd;
        }
    }

    cancelForm() {
        this.form.remove();
    }

    validateInput() {
        let inputs = this.form.querySelectorAll("[data-val='true']");

        inputs.forEach(input => {
            input.addEventListener("change", () => {
                let formInputValidator = new FormInputValidator(input);
                formInputValidator.validate();
            });
        });
    }

    validateInputsOnSubmit() {
        let isCorrect = true;
        let inputs = document.querySelectorAll("[data-val='true']");

        inputs.forEach(input => {
            let formInputValidator = new FormInputValidator(input);
            isCorrect = formInputValidator.validate() && isCorrect;
        });

        return isCorrect;
    }

    changeToPermanentForm() {
        this.form.classList.add("wait-to-add");
        this.form.classList.add("book");
        this.form.classList.remove("temporary-block");

        this.form.querySelector("h1").textContent = this.elementTypeName + " на сохранение";

        let properties = this.form.querySelectorAll(".form-group");

        properties.forEach(property => {
            property.classList.add("property");
            property.classList.remove("form-group");

            let label = property.querySelector("label");
            let divLabel = document.createElement("div");
            divLabel.classList.add("property-name");
            divLabel.textContent = label.textContent;
            property.append(divLabel);
            label.remove();

            let input = property.querySelector("input,select");
            let divInput = document.createElement("div");
            divInput.textContent = input.value;

            property.append(divInput);
            input.remove();
        });

        let errors = this.form.querySelectorAll(".error");
        errors.forEach(error => error.remove());

        let buttons = this.form.querySelectorAll("button");
        buttons.forEach(button => button.remove());
    }
}
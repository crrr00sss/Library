﻿let dataLibraryAdder = new DataLibraryAdder("author", "Автор");
dataLibraryAdder.start();

let dataLibraryUpdater = new DataLibraryUpdater("Вы действительно хотите удалить автора? Все его книги будут удалены тоже!", "author");
dataLibraryUpdater.start();
﻿class FormInputValidator {
    constructor(element) {
        this.element = element;
    }

    validateState(message, predicate) {

        let errorLabel = document.querySelector("#" + this.element.name + "-error-label");
        this.element.classList.remove("invalidInput");

        if (typeof predicate == "function" && predicate()) {
            errorLabel.classList.remove("errorLabel");
            return true;
        }
        else {
            this.element.classList.add("invalidInput");
            errorLabel.textContent = message;
            errorLabel.classList.add("errorLabel");
            return false;
        }
    }

    validateInputRequired() {
        let message = this.element.dataset.validateInputRequired;
        return this.validateState(message, () => this.element.value.length > 0);
    }

    validateAboveZero() {
        let message = "Значения должно быть больше нуля";
        return this.validateState(message, () => +this.element.value > 0);
    }

    validate() {
        let statesForCheck = this.element.dataset;

        for (const state in statesForCheck) {
            let validateFunction = this[state];

            if (typeof validateFunction == "function") {
                if (!this[state]()) {
                    return false;
                }
            }
        }

        return true;
    }
}
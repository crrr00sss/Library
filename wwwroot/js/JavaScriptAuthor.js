﻿insertAuthorData();

dataLibraryAdder = new DataLibraryAdder("book", "Книга");
dataLibraryAdder.start();

let dataLibraryUpdater = new DataLibraryUpdater("Вы действительно хотите удалить книгу?", "book", "author");
dataLibraryUpdater.start();

function insertAuthorData() {
    document.forms[0].lname.value = document.querySelector("#author-lname").textContent;
    document.forms[0].fname.value = document.querySelector("#author-fname").textContent;
    document.forms[0].patronymic.value = document.querySelector("#author-patronymic").textContent;
    let bdate = document.querySelector("#author-bdate").textContent;
    bdate = new Date(bdate).toISOString().substring(0, 10);
    document.forms[0].birthDate.value = bdate;
}

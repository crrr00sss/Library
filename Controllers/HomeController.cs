﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Model;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        readonly ILibraryDb _libraryDb;

        public HomeController(ILibraryDb libraryDb)
        {
            _libraryDb = libraryDb;
        }

        public IActionResult Index(bool isNotAddedAuthors)
        {
            var authors = _libraryDb.Authors.OrderBy(a => a.LastName).ToList();
            ViewBag.NotAddedAuthors = isNotAddedAuthors;

            return View(authors);
        }

        public IActionResult AuthorsInfo(int id, bool isNotAddedBooks)
        {
            var author = _libraryDb.GetAuthorById(id);

            ViewBag.Genres = _libraryDb.Genres;
            ViewBag.BirthDate = author.DateOfBirth.ToString("yyyy-MM-dd");
            ViewBag.NotAddedBooks = isNotAddedBooks;

            return View(author);
        }

        [HttpPost]
        public IActionResult ChangeAuthorInfo(string authorId, string lname, string fname, string patronymic, string newBooks, string birthDate)
        {
            newBooks ??= "[]";

            var authorIdInt = Convert.ToInt32(authorId);
            var books = JsonConvert.DeserializeObject<List<Book>>(newBooks);

            var isNotAddedBooks = _libraryDb.AddBooks(books, authorIdInt).Any();

            Author author = new();
            author.FirstName = fname;
            author.Patronymic = patronymic;
            author.LastName = lname;
            author.DateOfBirth = DateTime.Parse(birthDate);

            _libraryDb.ChangeAuthorInfo(authorIdInt, author);

            _libraryDb.Save();

            return RedirectToAction("AuthorsInfo", "Home", new { id = authorId, isNotAddedBooks });
        }

        public IActionResult Genres(bool isNotAddedGenres)
        {
            var genres = _libraryDb.Genres
                .OrderBy(g => g.Name)
                .ToList();
            ViewBag.NotAddedGenres = isNotAddedGenres;

            return View(genres);
        }

        [HttpPost]
        public IActionResult AddAuthor(string newAuthors)
        {
            var authorsToAdd = JsonConvert.DeserializeObject<List<Author>>(newAuthors);

            var isNotAddedAuthors = _libraryDb.AddAuthors(authorsToAdd).Any();

            _libraryDb.Save();

            return RedirectToAction("Index", "Home", new { isNotAddedAuthors });
        }

        [HttpPost]
        public IActionResult AddGenre(string newGenres)
        {
            var genresToAdd = JsonConvert.DeserializeObject<List<Genre>>(newGenres);

            var isNotAddedGenres = _libraryDb.AddGenres(genresToAdd).Any(); ;

            _libraryDb.Save();

            return RedirectToAction("Genres", "Home", new { isNotAddedGenres });
        }

        [HttpPost]
        public IActionResult DeleteAuthor(string authorId)
        {
            int id = Convert.ToInt32(authorId);

            _libraryDb.DeleteAuthor(id);

            _libraryDb.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteBook(string bookId, string authorId)
        {
            int id = Convert.ToInt32(bookId);

            _libraryDb.DeleteBook(id);

            _libraryDb.Save();

            return RedirectToAction("AuthorsInfo", "Home", new { id = authorId });
        }

        [HttpPost]
        public IActionResult DeleteGenre(string genreId)
        {
            int id = Convert.ToInt32(genreId);

            _libraryDb.DeleteGenre(id);

            _libraryDb.Save();

            return RedirectToAction("Genres");
        }

        public IActionResult GenreBooks(string id)
        {
            var genreId = Convert.ToInt32(id);
            var books = _libraryDb
                .GetBooksByGenre(genreId)
                .OrderBy(b => b.Title);

            ViewBag.GenreName = _libraryDb.GetGenreById(genreId).Name;

            return View(books);
        }

    }
}
